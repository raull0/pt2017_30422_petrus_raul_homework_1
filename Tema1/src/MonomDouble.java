
public class MonomDouble extends Monom {

	public MonomDouble(Double coef, Double grade) {
		super(coef, grade);
		// TODO Auto-generated constructor stub
	}
	
	public MonomDouble Integrate(){
		MonomDouble a = new MonomDouble(0.0,0.0);
		a.coef=this.coef.doubleValue()/(this.grade.doubleValue()+1);
		a.grade=this.grade.doubleValue()+1;
		return a;
	}
	public String toString()
	{
		return this.coef.floatValue() + "x^" + this.grade;
	}

}
