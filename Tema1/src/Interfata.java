import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Interfata extends JPanel {
	public static void main(String[] args) {
		ArrayList<MonomInt> monomA = new ArrayList<>();
		ArrayList<MonomInt> monomZ = new ArrayList<>();
	ArrayList<MonomInt> monomB = new ArrayList<>();
	MonomInt B=new MonomInt(1,4);
	MonomInt c=new MonomInt(2,4);
	MonomInt d=new MonomInt(1,2);
	MonomInt e=new MonomInt(2,1);
	MonomInt f= new MonomInt(6,1);
	MonomInt g=new MonomInt(-1,0);
	MonomInt g1=new MonomInt(2,0);
	monomA.add(B);
	//monomA.add(d);
	monomA.add(e);
	monomA.add(g1);
	//monomB.add(c);
	monomB.add(f);
	monomB.add(g);
	Polynomial A=new Polynomial(monomA,4);
	Polynomial C=new Polynomial(monomB,4);
	Polynomial Adunare= new Polynomial(monomZ,0);
	Polynomial Scadere= new Polynomial(monomZ,0);
	Polynomial Z=new Polynomial(monomZ,0);
	Operatii operatii=new Operatii();
	Adunare.copy(C);
	A.sortarePol();
	C.sortarePol();
	System.out.println("A=  "  + A);
	System.out.println("C=  " + C);
	Z=operatii.adunarePol(A, Adunare);
	//System.out.println(C);
	System.out.println("Z=  " +Z);
	Z=operatii.scaderePol(A, C);
	Scadere.copy(C);
	Z.sortarePol();
	System.out.println(Z);
	//System.out.println(C);
	//System.out.println(A);
	Z=operatii.inmultire(A, C);
	System.out.println(Z);
	//System.out.println("C=  " + C);
	//System.out.println(A);
	Z=operatii.impartire(A,C);
	System.out.println(Z.monoms5);
	//System.out.println(Z);
	
	
	
	
	JFrame frame = new JFrame("Simple Frame");
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setSize(600, 500);
	

	JPanel panel = new JPanel();
	panel.setLayout(null);
	panel.setBounds(0, 0, 600, 300);
	panel.setBackground(Color.WHITE);

	JLabel l = new JLabel("1ST POLYNOMIAL: ");
	l.setBounds(100, 30, 200, 20);
	panel.add(l);
	
	JLabel l1 = new JLabel("2ND POLYNOMIAL: ");
	l1.setBounds(100, 100, 200, 20);
	panel.add(l1);
	
	
		JTextField tf1 = new JTextField();
	tf1.setBounds(140, 120, 300, 30);
	panel.add(tf1);
	
	JTextField tf2 = new JTextField();
	tf2.setBounds(140, 50, 300, 30);
	panel.add(tf2);
	
	JButton B1 = new JButton("ADD");
	B1.setBounds(50, 180, 100, 40);
	panel.add(B1);
	
	B1.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent arg0) {
		//	System.out.println(A.Adunare(C).toString()); 
			
			
		}});
	
	JButton B2 = new JButton("SUBSTRACT");
	B2.setBounds(50, 240, 100, 40);
	panel.add(B2);
	
	B2.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			//System.out.println(A.Scadere(C).toString()); 
			
		}});
	
	JButton B3 = new JButton("MULTIPLY");
	B3.setBounds(240, 180, 100, 40);
	panel.add(B3);
	
	JButton B4 = new JButton("DIVIDE");
	B4.setBounds(240,240, 100, 40);
	panel.add(B4);
	
	JButton B5 = new JButton("INTEGRATE");
	B5.setBounds(430, 180, 100, 40);
	panel.add(B5);
	
	JButton B6 = new JButton("DIFFERENCIATE");
	B6.setBounds(430, 240, 100, 40);
	panel.add(B6);
	
	JTextField tf3 = new JTextField();
	tf3.setBounds(140, 320, 300,100);
	panel.add(tf3);
	
	frame.setContentPane(panel);
	frame.setVisible(true);
	panel.setVisible(true);
	}
}
