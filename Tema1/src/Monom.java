
public class Monom {
	protected Number coef;
	protected Number grade;
	public Monom(Number coef, Number grade){
		this.coef=coef;
		this.grade=grade;
	}
	public Monom(Monom e){
		this.grade=e.grade;
		this.coef=e.coef;
	}
	public Number getCoef() {
		return coef;
	}
	public void setCoef(Number coef) {
		this.coef = coef;
	}
	public Number getGrade() {
		return grade;
	}
	public void setGrade(Number grade) {
		this.grade = grade;
	}
	public String toString(){
		return this.coef + "x^" + this.grade;
	}
		
		
		
	}

