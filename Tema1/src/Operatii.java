import java.util.ArrayList;
import java.util.Iterator;

public class Operatii {
	public Polynomial adunarePol(Polynomial A, Polynomial B) {
		ArrayList<MonomInt> monoms1 = new ArrayList<>();

		Polynomial rez = new Polynomial(monoms1, 0);
		// Polynomial A=new Polynomial(C);//copie
		// Polynomial B=new Polynomial(D);//copie
		Iterator<MonomInt> i = A.monoms.iterator();
		while (i.hasNext()) {

			MonomInt m = new MonomInt(0, 0);
			m.copy(i.next());
			Iterator<MonomInt> j = B.monoms.iterator();
			while (j.hasNext()) {
				MonomInt n = new MonomInt(0, 0);
				n = j.next();
				if (m.getGrade().intValue() == n.getGrade().intValue() && m.getCoef().intValue() != 0
						&& n.getCoef().intValue() != 0) {
					m.setCoef(m.getCoef().intValue() + n.getCoef().intValue());
					n.setCoef(0);
				}
			}
			rez.AddMonom(m);
		}
		Iterator<MonomInt> k1 = B.monoms.iterator();
		while (k1.hasNext()) {
			MonomInt m = new MonomInt(0, 0);
			m.copy(k1.next());
			if (m.getCoef().intValue() != 0)
				rez.AddMonom(m);
		}
		rez.setGrade();
		rez.sortarePol();
		return rez;

	}

	public Polynomial scaderePol(Polynomial A, Polynomial B) {
		ArrayList<MonomInt> monoms1 = new ArrayList<>();

		Polynomial rez = new Polynomial(monoms1, 0);
		int k = 0;
		Iterator<MonomInt> i = A.monoms.iterator();
		while (i.hasNext()) {
			k++;
			int l = 0;
			MonomInt m = new MonomInt(0, 0);
			m.copy(i.next());
			Iterator<MonomInt> j = B.monoms.iterator();
			while (j.hasNext()) {
				MonomInt n = new MonomInt(0, 0);
				n = j.next();

				if (m.getGrade().intValue() == n.getGrade().intValue() && m.getCoef().intValue() != 0
						&& n.getCoef().intValue() != 0 && l > k) {
					m.setCoef(m.getCoef().intValue() - n.getCoef().intValue());
					n.setCoef(0);

				}
			}
			rez.AddMonom(m);
		}
		Iterator<MonomInt> k1 = B.monoms.iterator();
		while (k1.hasNext()) {
			MonomInt m = new MonomInt(0, 0);
			m.copy(k1.next());
			if (m.getCoef().intValue() != 0)
				m.setCoef(-m.getCoef().intValue());

			rez.AddMonom(m);
		}
		rez.setGrade();
		rez.sortarePol();
		return rez;
	}

	public Polynomial inmultire(Polynomial A, Polynomial B) {
		ArrayList<MonomInt> monoms1 = new ArrayList<>();
		
		Polynomial rez = new Polynomial(monoms1, 0);
		Iterator<MonomInt> i = A.monoms.iterator();
		
		while (i.hasNext()) {
			MonomInt m = new MonomInt(0, 0);
			m.copy(i.next());
			
			Iterator<MonomInt> j = B.monoms.iterator();
			while (j.hasNext()) {
				
				MonomInt n = new MonomInt(0, 0);
				MonomInt s = new MonomInt(0, 0);
				n = j.next();
				
					s.setCoef(m.getCoef().intValue()*n.getCoef().intValue());
					s.setGrade(m.getGrade().intValue()+n.getGrade().intValue());
					rez.AddMonom(s);
					
			}
			
		}
		rez.sortarePol();
		rez.setGrade();
		return rez;
	}
	public Polynomial impartire(Polynomial A, Polynomial B) {
		ArrayList<MonomInt> monoms1 = new ArrayList<>();
		
		Polynomial rez = new Polynomial(monoms1, 0);
		Iterator<MonomInt> i = A.monoms.iterator();
		
		while (i.hasNext()) {
			MonomInt m = new MonomInt(0, 0);
			m.copy(i.next());
			
			Iterator<MonomInt> j = B.monoms.iterator();
			while (j.hasNext()) {
				
				MonomInt n = new MonomInt(0, 0);
				MonomDouble s = new MonomDouble(0.0, 0.0);
				n = j.next();
				
					s.setCoef(m.getCoef().doubleValue()/n.getCoef().doubleValue());
					s.setGrade(m.getGrade().doubleValue()-n.getGrade().doubleValue());
					rez.monoms5.add(s);
					
			}
			
		}
		rez.sortarePol();
		rez.setGrade();
		return rez;
	}
}
