import java.util.ArrayList;
import java.util.Iterator;

public class Polynomial {
	ArrayList<MonomInt> monoms = new ArrayList<>();
	ArrayList<MonomDouble> monoms5 = new ArrayList<>();
	public int grad;

	public Polynomial(ArrayList<MonomInt> monoms, int grade) {
		this.monoms = monoms;
		this.grad = grade;
	}

	public Polynomial(Polynomial other) {
		this.monoms = other.monoms;
		this.grad = other.grad;

	}

	public void copy(Polynomial C) {
		Iterator<MonomInt> i = C.monoms.iterator();
		while (i.hasNext()) {
			MonomInt m = new MonomInt(0, 0);
			m.copy(i.next());
			this.monoms.add(m);
		}

	}

	public void setGrade() {
		Iterator<MonomInt> i = this.monoms.iterator();
		int maxi = this.monoms.get(0).getGrade().intValue();
		while (i.hasNext()) {
			MonomInt m = i.next();
			if (m.getGrade().intValue() > maxi)
				maxi = m.getGrade().intValue();
		}
		this.setGrad(maxi);
	}

	public void makePol() {
		int k = 0;
		Iterator<MonomInt> i = this.monoms.iterator();
		while (i.hasNext()) {
			MonomInt m = i.next();
			Iterator<MonomInt> j = this.monoms.iterator();
			k++;
			int l = 0;
			while (j.hasNext()) {
				l++;
				MonomInt n = new MonomInt(0, 0);
				n = j.next();
				if (m.getGrade().intValue() == n.getGrade().intValue() && m.getCoef().intValue() != 0
						&& n.getCoef().intValue() != 0 && l > k) {
					m.setCoef(m.getCoef().intValue() + n.getCoef().intValue());
					n.setCoef(0);
				}
			}
		}
	}

	public void faraZero() {
		for(int i = 0 ; i< this.monoms.size(); i++){
			if (this.monoms.get(i).getCoef().intValue()==0)
				this.monoms.remove(this.monoms.get(i));
		}
	}

	public void sortarePol() {
		Iterator<MonomInt> i = this.monoms.iterator();
		while (i.hasNext()) {
			MonomInt m = i.next();
			Iterator<MonomInt> j = this.monoms.iterator();
			while (j.hasNext()) {
				MonomInt n = j.next();
				if (m.getGrade().intValue() > n.getGrade().intValue()) {
					MonomInt aux = new MonomInt(0, 0);
					aux.setCoef(m.getCoef().intValue());
					aux.setGrade(m.getGrade().intValue());
					m.setCoef(n.getCoef().intValue());
					m.setGrade(n.getGrade().intValue());
					n.setCoef(aux.getCoef().intValue());
					n.setGrade(aux.getGrade().intValue());
				}
			}
		}
		this.makePol();
		this.faraZero();
	}

	public void AddMonom(MonomInt m) {
		this.monoms.add(m);
	}

	public String toString() {

		return "Polynomial [monoms=" + monoms + " grad=" + grad + "]";
	}

	public int getGrad() {
		return grad;
	}

	public void setGrad(int grad) {
		this.grad = grad;
	}
}