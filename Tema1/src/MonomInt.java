
public class MonomInt extends Monom{
	private Integer coef, grade;
	public MonomInt(Integer coef, Integer grade) {
		super(coef, grade);
		
			
	}
	public void copy(MonomInt other){
			this.setCoef(other.getCoef().intValue());
			this.setGrade(other.getGrade().intValue());
	}

	public MonomInt diff(){
		MonomInt a = new MonomInt(0,0);
		a.coef=this.coef.intValue()*this.grade.intValue();
		a.grade=this.grade.intValue()-1;
		return a;
	}
	
}
